import React, { Component } from 'react';
import './index.css'

class UserDetailCover extends Component{
    constructor(){
        super();
        this.state = {
            cover: "http://info.mzalendo.com/media_root/images/469BBEA9-486C-E211-A5C8-005056870012-2013-03-01T17-10-29_1.jpg"
        }
    }
    render(){
        const { firstName, lastName } = this.props;
        return(
          <div className="UserDetailCover">
                <img
                src={ this.state.cover }
                className="UserDetailCover-img"
                alt="avatar"/>
                <div>
                    <h2 className="UserDetailCover-name">{ firstName + " " + lastName }</h2>
                </div>
          </div>
        );
      }
    }

export default UserDetailCover;